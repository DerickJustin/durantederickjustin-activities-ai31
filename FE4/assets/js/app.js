
var ctx = document.getElementById('barChart').getContext('2d');

var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [300, 200, 100, 115, 65],
      backgroundColor: 'rgb(0,0,128)',
      
    },
    {
      label: 'Returned Books',
      data: [210, 250, 180, 90, 180],
      backgroundColor: 'rgb(255,0,0)',
    }
  ],
  },
  options: {
    responsive: true,
    legend: {
      position: 'right',
      align: 'start',
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        },
        
      }],
      xAxes: [{
        gridLines: {
           display: false
        }
     }]
    }
  }
});

var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'doughnut',
  data: {
    datasets: [{
        data: [23.1, 38.5, 7.7, 15.4,15.4],
        backgroundColor: [
          'rgba(27, 64, 230, .8)',
          'rgb(255,0,0)',
          'rgba(255,69,0)',
          'rgba(34,139,34)',
          'rgb(153,50,204)',
        ],
    }],

    labels: [
        'History',
        'Sci-Fi',
        'Information Technology',
        'Algebra',
        'English Literature',
    ]
  },
  options: {
    legend: {
      position: 'right',
      align: 'start',
    }
  }
});


